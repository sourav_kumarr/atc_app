import React, { Component } from 'react'
import { View } from 'react-native'
import { Provider } from 'react-redux'
import reducers from './src/Reducers'
import { applyMiddleware, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'
// import rootSaga from './src/Sagas'
import AppNavigation from './src/Navigation'
import { composeWithDevTools } from 'redux-devtools-extension'

// const sagaMiddleware = createSagaMiddleware()

const appStore = createStore(reducers);
// sagaMiddleware.run(rootSaga)

export default class App extends Component {
    render () {
        return (

            <Provider store={appStore}>
                    <View style={{flex: 1}}>
                        <AppNavigation />
                    </View>
            </Provider>
        );
    }
}