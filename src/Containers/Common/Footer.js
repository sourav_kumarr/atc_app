import React,{ Component }from 'react'
import {View,Text,TouchableOpacity,Image} from 'react-native'
import Styles from '../Common/style'
import Color from '../Common/color'
class Footer extends Component{
   render() {
       return(
       <View style={[Styles.height_25,Styles.zIndex_1]}>
       <View style={[Styles.footerContainer,Styles.row,Styles.marginTop_4]}>
           <View style={[Styles.width_40,Styles.left]}>
               <Text style={{'color':Color.black}}>LEVEL</Text>
               <Text style={{'color':Color.black}}>BEGINNER</Text>
           </View>

           <View style={[Styles.width_60,Styles.right]}>
            <TouchableOpacity style={Styles.paddingRight_2}>
                <View style={[Styles.upgrade,Styles.center]}>
                    <Text style={Styles.weight}>UPGRADE</Text>
                </View>
            </TouchableOpacity>
           </View>
       </View>

           <View style={[Styles.width_40,Styles.center]}>

               <TouchableOpacity style={[Styles.mike,Styles.center]}>
                <Image source={require('../../Assets/Imgs/mike.png')} style={Styles.icon} resizeMode={'contain'}/>
               </TouchableOpacity>

           </View>

       </View>
       )
   }
}

export default Footer;