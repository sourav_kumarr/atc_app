import React,{ Component }from 'react'
import {View,Text,Image,ImageBackground,TouchableOpacity} from 'react-native'
import {responsiveWidth,responsiveHeight} from 'react-native-responsive-dimensions'
import Styles from './Common/style';
import Footer from '../Containers/Common/Footer'
export default class MainContainer extends Component{

    render() {
        return(
            <View style={[Styles.container,{zIndex:0}]}>
                <View style={[Styles.height_75,Styles.zIndex_0]}>
                <ImageBackground style={Styles.headerContainer} source={require('../Assets/Imgs/logo.jpg')}>
                    <View style={[Styles.width_100,Styles.padding_2,Styles.marginTop_2,Styles.row]}>
                        <TouchableOpacity style={[Styles.width_10,Styles.center]}>
                            <Image source={require('../Assets/Imgs/menu.png')} style={Styles.icon} resizeMode={'contain'}/>
                        </TouchableOpacity>
                        <View style={[Styles.width_80,Styles.center]}>
                            <Text style={[Styles.textColorWhite,Styles.fontSize_3]}>KGJT Grand Junction</Text>
                        </View>
                        <TouchableOpacity style={[Styles.width_10,Styles.left]}>
                            <Image source={require('../Assets/Imgs/setting.png')} style={Styles.icon} resizeMode={'contain'}/>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
                <View style={[Styles.middleContainer,Styles.center]}>
                    <Text style={[Styles.textColorPurple,Styles.fontSize_2]}>CESSNA N734CP</Text>
                    <Text style={[Styles.textColorWhite,Styles.marginTop_2,Styles.fontSize_2,Styles.weight]}>You’re at Western Air, ready to taxi. </Text>
                    <Text style={[Styles.textColorPurple,Styles.marginTop_2,Styles.fontSize_2]}>INFORMATION NOVEMBER</Text>
                </View>
                </View>

                <Footer/>
            </View>

        )
    }
}
