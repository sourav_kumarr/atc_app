export default {
    white: '#fff',
    black: '#000',
    grey: '#ccc',
    red: '#f00',
    navyBlue: '#3171DD',
    darkBlue: '#005b9c',
    yellow:'#FCFF33',
    lightRed:'#c00113',
    darkRed:'#e1041b',
    purple:'#BDAFB8',
    mikeColor:'#C15564'
};